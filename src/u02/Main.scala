package u02

import com.sun.glass.ui.MenuItem.Callback

object Main extends App {
  println("Hello World")

  val pair : Int => String = {
    case x if x % 2 == 0 => "Even"
    case _ => "Odd"
  }

  println(pair(2));
  println(pair(3));

  def pair_method (n:Int):String = n match{
    case n if n % 2 == 0 => "Even"
    case _ => "Odd"
  }

  println(pair_method(2));
  println(pair_method(3));

  def neg_method(predicate : String => Boolean): String => Boolean = {
    myString:String => !predicate(myString)
  }

  val neg : (String => Boolean) => (String => Boolean) = (p) => (s=>(!p(s)))

  def negGeneric[A](predicate : A => Boolean): A => Boolean = {
    x:A => !predicate(x)
  }

  val majorityCheckCurryed : Integer => Integer => Integer => Boolean = x => y => z => x <= y && y <= z
  val majorityCheck : (Int,Int,Int) => Boolean = {
    (x,y,z) => x <= y && y <= z
  }
  def majorityCheckMethod (x:Int,y:Int,z:Int):Boolean = {
    x<= y && y<= z
  }
  def majorityCheckMethodCurryed (x:Int)(y:Int)(z:Int):Boolean = {
    x<= y && y<= z
  }

  //Non è tail recursion, dopo la prima ricorsione viene eseguita una seconda ricorsione ed inoltre la somma, quindi non è l'ultima operazione
  def fibonacci (n:Int):Int = n match{
    case 0 => 0
    case 1 => 1
    case _ => fibonacci(n-1) + fibonacci(n-2)
  }

  sealed trait Shape
  object Shape {
    case class Rectangle(l1 : Double, l2:Double) extends Shape
    case class Circle(r : Double) extends Shape
    case class Square(l1 : Double) extends Shape
    def area(s : Shape): Double = s match{
      case Rectangle(l1,l2) => l1 * l2
      case Circle(r) => 3.14 * r * r
      case Square(l1) => l1 * l1
    }
    def perimeter(s : Shape): Double = s match{
      case Rectangle(l1,l2) => (l1+l2) * 2
      case Circle(r) => 3.14 * 2 * r
      case Square(l1) => l1 * 4
    }
  }

  val empty : String => Boolean = _ == ""
  val notempty = neg_method(empty);
  println(notempty("foo"))// true
  println(notempty(""))
  var notempty2 = negGeneric(empty);
  println(notempty2("foo"))
  println(notempty2(""))
  var notempty3 = neg(empty)
  println(notempty3("foo"))// true
  println(notempty3(""))
  println(majorityCheckCurryed(2)(3)(4))
  println(majorityCheckCurryed(2)(4)(3))
  println(majorityCheckCurryed(4)(3)(2))
  println(majorityCheck(2,3,4))
  println(majorityCheck(2,4,3))
  println(majorityCheck(4,3,2))
  println(majorityCheckMethod(2,3,4))
  println(majorityCheckMethod(2,4,3))
  println(majorityCheckMethod(4,3,2))
  println(majorityCheckMethodCurryed(2)(3)(4))
  println(majorityCheckMethodCurryed(2)(4)(3))
  println(majorityCheckMethodCurryed(4)(3)(2))
  println(fibonacci(0))
  println(fibonacci(1))
  println(fibonacci(2))
  println(fibonacci(3))
  println(fibonacci(4))
  println(fibonacci(5))

  import Shape._
  println(area(Rectangle(17,11)))
  println(perimeter(Rectangle(17,11)))
  println(area(Square(9)))
  println(perimeter(Square(9)))
  println(area(Circle(15)))
  println(perimeter(Circle(15)))
}
